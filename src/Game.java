
import com.sun.xml.internal.ws.util.StringUtils;
import java.lang.String.*;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Game {
    private Board solution;
    private Board notSolved;

    /*public Game(Board solution, Board notSolved){
        this.solution = solution;
        this.notSolved = notSolved;
    }*/

    public Game(){};

    public Board getSolution(){
        return solution;
    }

    public Board getNotSolved(){
        return notSolved;
    }

    public void playTheGame() throws InterruptedException, MyFIleNotFoundException, MyWrongLevelException, MyNotNumericInputException {
        System.out.println("Witaj w grze Sudoku! Jeżeli chcesz otrzymać Sudoku do rozwiązania, musisz wybrać jego poziom.");
        System.out.println("Dostępne poziomy: ");
        System.out.println("-> Bardzo łatwy: 0 ");
        System.out.println("-> Łatwy: 1 ");
        System.out.println("-> Średni: 2 ");
        System.out.println("-> Trudny: 3 ");
        System.out.println("Aby dokonać poziomu podaj odpowiedni numer: ");

        String sWhatever;

        Scanner scanIn = new Scanner(System.in);
        sWhatever = scanIn.nextLine();

        if(!(Game.isNumeric(sWhatever))){
            throw new MyNotNumericInputException("Wpisana wartość nie jest numeryczna!");
        }

        int lvl = Integer.parseInt(sWhatever);
        scanIn.close();

        if(lvl < 0 || lvl > 3){
            throw new MyWrongLevelException("Podano niepoprawny numer poziomu!");
        }

        SudokuReader reader = new SudokuReader();

        reader.getBoard(Level.getLevel(lvl));
        System.out.println("Wybrałeś poziom: "+lvl);
        Board solved = reader.board;
        Board notSolved = reader.notSolved;
        solution = solved;
        this.notSolved = notSolved;
        System.out.println("Oto Twoje Sudoku do rozwiązania: ");
        notSolved.printBoard();
        System.out.println("Rozwiązanie zostanie wyświetlone za " + (lvl+1)+" min. ");

        TimeUnit.SECONDS.sleep((lvl+1)*60);
        System.out.println("\nHere is the answer!: ");
        solved.printBoard();

    }

    public static boolean isNumeric(String str) {
        if (str == null) {
            return false;
           }
        int sz = str.length();
        for (int i = 0; i < sz; i++) {
            if (Character.isDigit(str.charAt(i)) == false) {
                return false;
            }
        }
        return true;
    }
}
