
public enum Level {
    VeryEasy, Easy, Medium, Hard;

    public static Level getLevel(int i){
        switch(i){
            case 0: return VeryEasy;
            case 1: return Easy;
            case 2: return Medium;
            case 3: return Hard;
        }
        return null;
    }

    public double getDouble() {
        switch(this){
            case VeryEasy: return 0.3;
            case Easy: return 0.4;
            case Medium: return 0.55;
            case Hard: return 0.6;
        }
        return 0.5;
    }
}
