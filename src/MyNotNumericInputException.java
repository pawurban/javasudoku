public class MyNotNumericInputException extends Exception{

    public MyNotNumericInputException(String message) {
        super(message);
    }

}