
public class Board {
    private Square [] squares;

    public Board (Square[] squares){
        this.squares = squares;
    }

    public Board(){
    }

    public Square[] getSquares(){
        return squares;
    }

    public void printBoard(){
        System.out.println("\n ----------------------- ");
        for(int i = 1; i <= 7; i+=3){
            this.squares[i-1].printThreeSquaresLineByLine(this.squares[i+1-1],this.squares[i+2-1]);
            System.out.println("\n ----------------------- ");
        }
    }
}
