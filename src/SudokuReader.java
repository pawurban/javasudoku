
import java.io.FileNotFoundException;
import java.io.File;
import java.util.Random;
import java.util.Scanner;

public class SudokuReader {
    public Board board;
    public Board notSolved;

    public Board getBoard(Level lvl) throws MyFIleNotFoundException {
        int[] sudoku = new int[81];

        Random rand = new Random();
        int fileNo = rand.nextInt(9);
        String sudokuFileName = "s" + fileNo + ".txt";

        try {
            Scanner scanner = new Scanner(new File(sudokuFileName));
            int i = 0;
            while (scanner.hasNextInt()) {
                sudoku[i++] = scanner.nextInt();
            }
        } catch (FileNotFoundException ex){
            System.out.println("PLIK NIE ZOSTAŁ ZNALEZIONY!");
            throw new MyFIleNotFoundException("Nie znaleziono pliku!");
        }

        Random random = new Random();
        Field [][] fields = new Field[9][9];
        Field [][] fieldsX  = new Field[9][9];
        int ptr = 0;
        for(int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if((random.nextDouble())>lvl.getDouble()) {
                    fields[i][j] = fieldsX[i][j]= new Field(Number.toNum(sudoku[ptr++]));
                } else {
                    fields[i][j] = new Field(Number.toNum(sudoku[ptr++]));
                    fieldsX[i][j]= new Field(Number.Unknown);
                }
            }
        }

        Square [] squares = new Square[9];
        Square [] squaresX = new Square[9];
        for(int i = 0; i < 9; i++) {
            squares[i] = new Square(fields[i]);
            squaresX[i] = new Square(fieldsX[i]);
        }
        board = new Board(squares);
        notSolved = new Board(squaresX);
        return board;
    }
}
