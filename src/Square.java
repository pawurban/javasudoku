
public class Square {
    private Field [] fields;

    public Square (Field [] fields){
        this.fields = fields;
    }

    public Field[] getFields(){
        return fields;
    }

    /*public void printSquare(){
        for(int i = 0; i < 9; i++) {
            this.fields[i].printField();
            if ((i + 1) % 3 == 0)
                System.out.println("\n");
        }
    }*/
    private void printSquareLine(int line){
        if(line == 1){
            for(int i = 0; i < 3; i++){
                this.fields[i].printField();
            }
        }
        else if(line == 2) {
            for(int i = 3; i < 6; i++){
                this.fields[i].printField();
            }
        }
        else if(line == 3){
            for(int i = 6; i < 9; i++){
                this.fields[i].printField();
            }
        }
    }

    public void printThreeSquaresLineByLine(Square two, Square three){
        for(int i = 1; i <= 3; i++){
            System.out.print("| ");this.printSquareLine(i); System.out.print("| ");
            two.printSquareLine(i);System.out.print("| ");
            three.printSquareLine(i);System.out.print("| ");
            if(i != 3)
                System.out.println();
        }
    }

}
