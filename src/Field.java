
public class Field {
    private final Number number;

    public Field (Number number){
        this.number = number;
    }

    public Number getNumber(){
        return number;
    }

    public void printField(){
        if(Number.toInt(number) == 0){
            System.out.print("_ ");
        }else {
            System.out.print(Number.toInt(getNumber()) + " ");
        }
    }
}
