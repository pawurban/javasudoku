
public class MyWrongLevelException extends Exception{

    public MyWrongLevelException(String message) {
        super(message);
    }

}