import java.io.IOException;

public class Main {
    public static void main(String[] args) throws InterruptedException, IOException, MyFIleNotFoundException, MyWrongLevelException, MyNotNumericInputException {

        Game game = new Game();
        game.playTheGame();

    }
}
