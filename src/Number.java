public enum Number {
    One, Two, Three, Four, Five, Six, Seven, Eight, Nine, Unknown;


    public static int toInt(Number number){
        switch(number) {
            case One: return 1;
            case Two: return 2;
            case Three: return 3;
            case Four: return 4;
            case Five: return 5;
            case Six: return 6;
            case Seven: return 7;
            case Eight: return 8;
            case Nine: return 9;
            case Unknown: return 0;
        }
        return -1;
    }

    public static Number toNum(int x){
        switch(x){
            case 0: return Unknown;
            case 1: return One;
            case 2: return Two;
            case 3: return Three;
            case 4: return Four;
            case 5: return Five;
            case 6: return Six;
            case 7: return Seven;
            case 8: return Eight;
            case 9: return Nine;
        }
        return null;
    }
}
